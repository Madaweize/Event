<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Event;

/**
 * Description of Emitter
 *
 * @author koby
 */
class Emitter {
    /**
     *
     * @var Emmiter 
     */
    protected static $_instance;
    
    /**
     *Enregistre la liste des ecouteurs
     * @var array
     */
    protected $listeners=[];
    


    /**
     * Permet de recuperer une instance de l'emeteur(singleton)
     * @return \Event\Emitter
     */
    public static function getInstance(): Emitter{
        if(self::$_instance==NULL){
            self::$_instance=new self();
        }
        
        return self::$_instance;
    }
    
    /**
     * Permet d'ecouter un evenement
     * @param string  Nom de l'evenement
     * @param \Event\callable $callable
     */
    public function on(string $event,callable $callable){
       if(!$this->haslistener($event)){
          $this->listeners[$event]=[];
       }
       
          $this->listeners[$event][]=$callable;
       
          
    }
    
    
    /**
     * Envoi d'un evenement
     * @param string $event Nom de l'evenement
     * @param type $args  c'est un spread operator :c'est a dire on attend 0 ou plusieurs parametres
     */
    public function emit(string $event,...$args){
       if($this->haslistener($event)){
           foreach ($this->listeners[$event] as $listener) {
               call_user_func_array($listener, $args);
           }
       }
    }
   
    
    /**
     * 
     * @param type $event
     * @return bool
     */
    private function haslistener(string $event): bool{
        return array_key_exists($event, $this->listeners);
    }
    
    
    
    
  
}
